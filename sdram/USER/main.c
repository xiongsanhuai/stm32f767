#include "sys.h"
#include "delay.h"
#include "usart.h" 
#include "SEGGER_RTT.h"
#include "breath.h"
#include "rttsh.h"
#include "sdram.h"
#include "stdint.h"
#define DBG_TAG "main"
#include "dbg.h"

uint32_t *ptrbuff[255] EXSDRAM = {0};
uint32_t *g_ptest1 = NULL;
int main(void)
{
	uart_init(115200);		        //串口初始化
	ShellInit();
	BreathLed_Init();  /*呼吸灯初始化*/
	g_ptest1 = malloc(4);
	*g_ptest1 = 100;
	LOG_I("g_ptest1 = 0x%p",g_ptest1);
	LOG_I("*g_ptest1 = %d",*g_ptest1);
	while(1)
	{
		delay_ms(500);
	}
}



void SystemCLKInit(void)
{
	Cache_Enable();                 //打开L1-Cache
    HAL_Init();				        //初始化HAL库
    Stm32_Clock_Init(432,25,2,9);   //设置时钟,216Mhz 
    delay_init(216);                //延时初始化
}

void SDRAMtest(void)
{
	static uint8_t ucIndex = 0;
	uint32_t *puitest = NULL;
	puitest = malloc(4);
	*puitest = ucIndex;
	ptrbuff[ucIndex] = puitest;
	LOG_I("ptr addr : 0x%p",puitest);
	LOG_I("ptr data : %d",*puitest);
	ucIndex++;
}
RTT_INIT_EXPORT_0(SDRAMtest);

void SDRAMInfo(void)
{
	for(uint8_t i = 0; i < 255; i++)
	{
		if(ptrbuff[i] == NULL)
		{
			break;
		}
		else
		{
			LOG_I("Index : %d",i);
			LOG_I("addr : 0x%p",ptrbuff[i]);
			LOG_I("data : %d",*ptrbuff[i]);
		}
	}
	LOG_I("Above sdram info");
}
RTT_INIT_EXPORT_0(SDRAMInfo);

