#include "rttsh.h"
#define DBG_TAG "RttSH"
#include "dbg.h"
#include <string.h>
#include <stdio.h>

static uint8_t arrucSHbuff[SHELL_SIZE] = {0};

TIM_HandleTypeDef stTim7Handle;

uint8_t ShellInit(void)
{
	uint8_t ucRet = 0;
	Tim7_Init(500 - 1,10800 - 1);
	rt_kprintf("=>");
	return ucRet;
}

/*自定义段*/
void RttStartParm_0(void)
{
	return ;
}
RTT_INIT_EXPORT(RttStartParm_0, "0.end",0);

void RttEndParm_0(void)
{
	return ;
}
RTT_INIT_EXPORT(RttEndParm_0, "1.end",0);

void RttStartParm_1(int param1)
{
	return ;
}
RTT_INIT_EXPORT(RttStartParm_1, "2.end",1);

void RttEndParm_1(int param1)
{
	return ;
}
RTT_INIT_EXPORT(RttEndParm_1, "3.end",1);

void RttStartParm_2(int param1,int param2)
{
	return ;
}
RTT_INIT_EXPORT(RttStartParm_2, "4.end",2);

void RttEndParm_2(int param1,int param2)
{
	return ;
}
RTT_INIT_EXPORT(RttEndParm_2, "5.end",2);

void RttStartParm_3(int param1,int param2,int param3)
{
	return ;
}
RTT_INIT_EXPORT(RttStartParm_3, "6.end",3);

void RttEndParm_3(int param1,int param2,int param3)
{
	return ;
}
RTT_INIT_EXPORT(RttEndParm_3, "7.end",3);
/*自定义段*/

void test1(void)
{
	LOG_I("test1");
}
RTT_INIT_EXPORT_0(test1);

void test2(int num1)
{
	LOG_I("test2,%d",num1);
}
RTT_INIT_EXPORT_1(test2);

void test3(int num1,int num2)
{
	LOG_I("test3,%d, %d",num1,num2);
}
RTT_INIT_EXPORT_2(test3);

void test4(int num1,int num2,int num3)
{
	LOG_I("test4,%d,%d,%d",num1,num2,num3);
}
RTT_INIT_EXPORT_3(test4);

void FunExe(char *pname)
{
	volatile const struct rt_init_desc_0 *stDev = &__init_RTT_RttStartParm_0;
	stDev++;
	for(;stDev < &__init_RTT_RttEndParm_0;stDev++)
	{
		if(0x00 == strncmp(stDev->fn_name,pname,20))
		{
			stDev->fn();
			return;
		}
	}
	rt_kprintf("Don't find function\r\n");
}

void FunExe1(char *pname, int param1)
{
	volatile const struct rt_init_desc_1 *stDev = &__init_RTT_RttStartParm_1;
	stDev++;
	for(;stDev < &__init_RTT_RttEndParm_1;stDev++)
	{
		if(0x00 == strncmp(stDev->fn_name,pname,20))
		{
			stDev->fn(param1);
			return;
		}
	}
	rt_kprintf("Don't find function\r\n");
}

void FunExe2(char *pname, int param1,int param2)
{
	volatile const struct rt_init_desc_2 *stDev = &__init_RTT_RttStartParm_2;
	stDev++;
	for(;stDev < &__init_RTT_RttEndParm_2;stDev++)
	{
		if(0x00 == strncmp(stDev->fn_name,pname,20))
		{
			stDev->fn(param1,param2);
			return;
		}
	}
	rt_kprintf("Don't find function\r\n");
}

void FunExe3(char *pname, int param1,int param2,int param3)
{
	volatile const struct rt_init_desc_3 *stDev = &__init_RTT_RttStartParm_3;
	stDev++;
	for(;stDev < &__init_RTT_RttEndParm_3;stDev++)
	{
		if(0x00 == strncmp(stDev->fn_name,pname,20))
		{
			stDev->fn(param1,param2,param3);
			return;
		}
	}
	rt_kprintf("Don't find function\r\n");
}
static void RttDeal(uint8_t *pbuff, uint16_t ussize)
{
	char FunName[20] = {0};
	char Parm1[5] = {0};
	char Parm2[5] = {0};
	char Parm3[5] = {0}; /*参数个数*/
	int cParamNum = -1;
	uint8_t ucindex = 0; /*储存下标*/
	rt_kprintf(RTT_CTRL_RESET);
	rt_kprintf("%s",pbuff);
	if(0x00 == strncmp("?", (char *)pbuff, 1)|| 0x00 == strncmp("help", (char *)pbuff, 4))
	{
		const struct rt_init_desc_0 *stDev0 = &__init_RTT_RttStartParm_0;
		const struct rt_init_desc_1 *stDev1 = &__init_RTT_RttStartParm_1;
		const struct rt_init_desc_2 *stDev2 = &__init_RTT_RttStartParm_2;
		const struct rt_init_desc_3 *stDev3 = &__init_RTT_RttStartParm_3;
		stDev0++;
		stDev1++;
		stDev2++;
		stDev3++;
		rt_kprintf("Fun of no param\r\n");
		for(;stDev0 < &__init_RTT_RttEndParm_0;stDev0++)
		{
			rt_kprintf("Command:%s\r\n",stDev0->fn_name);
		}
		rt_kprintf("Fun of one param\r\n");
		for(;stDev1 < &__init_RTT_RttEndParm_1;stDev1++)
		{
			rt_kprintf("Command:%s\r\n",stDev1->fn_name);
		}
		rt_kprintf("Fun of two params\r\n");
		for(;stDev2 < &__init_RTT_RttEndParm_2;stDev2++)
		{
			rt_kprintf("Command:%s\r\n",stDev2->fn_name);
		}
		rt_kprintf("Fun of three params\r\n");
		for(;stDev3 < &__init_RTT_RttEndParm_3;stDev3++)
		{
			rt_kprintf("Command:%s\r\n",stDev3->fn_name);
		}
		rt_kprintf("=>");
		return;
	}
	else if(0x00 == strncmp("clear", (char *)pbuff, 5))
	{
		rt_kprintf(RTT_CTRL_CLEAR);
		rt_kprintf("=>");
		return;
	}
	for(uint8_t i = 0; i < ussize; i++)
	{
		if(0x0d == pbuff[i] || 0x0a == pbuff[i])
		{
			break;
		}
		else if(0x09 == pbuff[i] || 0x20 == pbuff[i]) /*等于空格或者tab*/
		{
			if(FunName[0] == 0x00) /*函数名前有空格字符，舍去*/
			{
				continue;
			}
			else if((i+1) < ussize && (0x09 != pbuff[i+1] || 0x20 != pbuff[i+1]) && (0x0d != pbuff[i+1] || 0x0a != pbuff[i+1])) /*空格后没有空格或者回车*/
			{
				ucindex = 0;
				cParamNum++; /*函数名后有空格，认为参数加1*/
				continue ;/*空格不记录*/
			}
			else
			{
				rt_kprintf("RTT shell other,%c\r\n",pbuff[i]);
			}
		}
		switch(cParamNum)
		{
			case -1:
				FunName[ucindex] = pbuff[i];
				ucindex++;
				break;
			case 0:
				Parm1[ucindex] = pbuff[i];
				ucindex++;
				break;
			case 1:
				Parm2[ucindex] = pbuff[i];
				ucindex++;
				break;
			case 2:
				Parm3[ucindex] = pbuff[i];
				ucindex++;
				break;
			default:
				rt_kprintf("Fuction isn't allow to use more param!\r\n");
				break;
		}
	}
	int Num1,Num2,Num3;
	switch(cParamNum)
	{
		case -1:
			FunExe(FunName);
			break;
		case 0:
			Num1 = atoi(Parm1);
			FunExe1(FunName,Num1);
			break;
		case 1:
			Num1 = atoi(Parm1);
			Num2 = atoi(Parm2);
			FunExe2(FunName,Num1, Num2);
			break;
		case 2:
			Num1 = atoi(Parm1);
			Num2 = atoi(Parm2);
			Num3 = atoi(Parm3);
			FunExe3(FunName,Num1, Num2, Num3);
			break;
		default:
			rt_kprintf("Other function\r\n");
			break;
	}
	rt_kprintf("=>");
}

static uint8_t Tim7_Init(uint16_t arr, uint16_t psc)
{
	uint8_t ucRet = 0;
	__HAL_RCC_TIM7_CLK_ENABLE();            //使能TIM7时钟
	stTim7Handle.Instance=TIM7;                          //通用定时器7
    stTim7Handle.Init.Prescaler=psc;                     //分频
    stTim7Handle.Init.CounterMode=TIM_COUNTERMODE_UP;    //向上计数器
    stTim7Handle.Init.Period=arr;                        //自动装载值
    stTim7Handle.Init.ClockDivision=TIM_CLOCKDIVISION_DIV1;//时钟分频因子
    ucRet = HAL_TIM_Base_Init(&stTim7Handle);
	
	HAL_NVIC_SetPriority(TIM7_IRQn,3,3);    //设置中断优先级，抢占优先级3，子优先级3
	HAL_NVIC_EnableIRQ(TIM7_IRQn);          //开启ITM7中断  
	
	if(ucRet)
	{
		LOG_E("Tim7 cfg err");
	}
    
    ucRet = HAL_TIM_Base_Start_IT(&stTim7Handle); //使能定时器3和定时器3更新中断：TIM_IT_UPDATE
	if(ucRet)
	{
		LOG_E("Tim7 start err");
	}
	return ucRet;
}

void ClearBuff(uint8_t *pbuff, uint16_t ussize)
{
	for(uint16_t i = 0; i < ussize; i++)
	{
		pbuff[i] = 0x00;
	}
}

void TIM7_IRQHandler(void)
{
	if(__HAL_TIM_GET_IT_SOURCE(&stTim7Handle, TIM_IT_UPDATE) !=RESET)
    {
		__HAL_TIM_CLEAR_IT(&stTim7Handle, TIM_IT_UPDATE);
		SEGGER_RTT_Read(0, arrucSHbuff, SHELL_SIZE); /*获取数据*/
		if(arrucSHbuff[0] != 0x00)
		{
			RttDeal(arrucSHbuff, SHELL_SIZE);
		}
		ClearBuff(arrucSHbuff, SHELL_SIZE);
	}
}

