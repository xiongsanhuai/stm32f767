#ifndef __RTTSH_H
#define __RTTSH_H
#include "SEGGER_RTT.h"
#include "sys.h"

#define SHELL_SIZE 32
#define USED 						__attribute__((used))
#define SECTION(x)                  __attribute__((section(x)))

typedef void (*init_fn_0)(void);
typedef void (*init_fn_1)(int);
typedef void (*init_fn_2)(int,int);
typedef void (*init_fn_3)(int,int,int);
struct rt_init_desc_0
{
	const char* fn_name;
	const init_fn_0 fn;
};
struct rt_init_desc_1
{
	const char* fn_name;
	const init_fn_1 fn;
};
struct rt_init_desc_2
{
	const char* fn_name;
	const init_fn_2 fn;
};
struct rt_init_desc_3
{
	const char* fn_name;
	const init_fn_3 fn;
};


#define RTT_INIT_EXPORT(fn, level,num) 												\
	const char _RTT_##fn##_name[] = #fn;										\
	USED const struct rt_init_desc_##num __init_RTT_##fn SECTION(".rtt_fn"level) = 	\
	{_RTT_##fn##_name, fn};														

	
#define RTT_INIT_EXPORT_0(fn)	RTT_INIT_EXPORT(fn,"1.",0)
#define RTT_INIT_EXPORT_1(fn)	RTT_INIT_EXPORT(fn,"3.",1)
#define RTT_INIT_EXPORT_2(fn)	RTT_INIT_EXPORT(fn,"5.",2)
#define RTT_INIT_EXPORT_3(fn)	RTT_INIT_EXPORT(fn,"7.",3)



extern uint8_t ShellInit(void);
extern void ClearBuff(uint8_t *pbuff, uint16_t ussize);

static uint8_t Tim7_Init(uint16_t arr, uint16_t psc);

#endif

