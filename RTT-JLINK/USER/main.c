#include "sys.h"
#include "delay.h"
#include "usart.h" 
#include "SEGGER_RTT.h"
#include "breath.h"
#include "rttsh.h"

int main(void)
{
    Cache_Enable();                 //打开L1-Cache
    HAL_Init();				        //初始化HAL库
    Stm32_Clock_Init(432,25,2,9);   //设置时钟,216Mhz 
    delay_init(216);                //延时初始化
	uart_init(115200);		        //串口初始化
	ShellInit();
	BreathLed_Init();  /*呼吸灯初始化*/
	while(1)
	{
		delay_ms(500);
	}
}
