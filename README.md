# stm32学习代码

#### 介绍
stm32f767hal库学习

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1. RTT-JLINK文件夹中代码为RTT-JLINK log输出模板，支持日志等级，可以通过RTT view输入参数。
2. SDRAM自动分配，可以通过malloc calloc realloc分配到此空间，也可以通过自定义段SDRAM。注意，通过自定义段分配，初始化的值无效，需要到程序中赋值初始化才行。
3. NandFlash中根据正点原子教程，重新规范了代码格式以及注释。
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
