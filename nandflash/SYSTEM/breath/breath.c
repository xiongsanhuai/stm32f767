#include "breath.h"

#define DBG_TAG "BreathLed"
#include "dbg.h"


TIM_HandleTypeDef stTim6Handle;


uint8_t BreathLed_Init(void)
{
	GPIO_InitTypeDef stGpioInit;
	__HAL_RCC_GPIOB_CLK_ENABLE();			//开启GPIOB时钟
	
	stGpioInit.Pin=GPIO_PIN_0; //PB0
    stGpioInit.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
    stGpioInit.Pull=GPIO_PULLUP;          //上拉
    stGpioInit.Speed=GPIO_SPEED_LOW;     //高速
    HAL_GPIO_Init(GPIOB,&stGpioInit);
	
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,GPIO_PIN_SET);	//PB0置1
	
	Tim6_Init(10000 - 1,10800 - 1);
	LOG_I("Breath Led Start!");
	return 0;
}

static uint8_t Tim6_Init(uint16_t arr, uint16_t psc)
{
	uint8_t ucRet = 0;
	__HAL_RCC_TIM6_CLK_ENABLE();            //使能TIM6时钟
	stTim6Handle.Instance=TIM6;                          //通用定时器6
    stTim6Handle.Init.Prescaler=psc;                     //分频
    stTim6Handle.Init.CounterMode=TIM_COUNTERMODE_UP;    //向上计数器
    stTim6Handle.Init.Period=arr;                        //自动装载值
    stTim6Handle.Init.ClockDivision=TIM_CLOCKDIVISION_DIV1;//时钟分频因子
    ucRet = HAL_TIM_Base_Init(&stTim6Handle);
	
	
	HAL_NVIC_SetPriority(TIM6_DAC_IRQn,3,3);    //设置中断优先级，抢占优先级1，子优先级3
	HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);          //开启ITM6中断   
	if(ucRet)
	{
		LOG_E("Tim6 cfg err");
	}
    
    ucRet = HAL_TIM_Base_Start_IT(&stTim6Handle); //使能定时器6和定时器6更新中断：TIM_IT_UPDATE
	if(ucRet)
	{
		LOG_E("Tim6 start err");
	}
	
	return ucRet;
}



uint8_t BreathToggle(void)
{
	HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_0);
	return 0;
}


void TIM6_DAC_IRQHandler(void)
{
	if(__HAL_TIM_GET_IT_SOURCE(&stTim6Handle, TIM_IT_UPDATE) !=RESET)
    {
      __HAL_TIM_CLEAR_IT(&stTim6Handle, TIM_IT_UPDATE);
		BreathToggle();
	}
}

