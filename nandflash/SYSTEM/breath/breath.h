#ifndef __HREATH_H
#define __HREATH_H
#include "sys.h"

extern uint8_t BreathLed_Init(void);
extern uint8_t BreathToggle(void);
static uint8_t Tim6_Init(uint16_t arr, uint16_t psc);
#endif

