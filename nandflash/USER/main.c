#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "SEGGER_RTT.h"
#include "breath.h"
#include "rttsh.h"
#include "nandflash.h"

#define DBG_TAG "Main"
#include "dbg.h"
int main(void)
{
    Cache_Enable();                 //打开L1-Cache
    HAL_Init();				        //初始化HAL库
    Stm32_Clock_Init(432,25,2,9);   //设置时钟,216Mhz
    delay_init(216);                //延时初始化
	uart_init(115200);		        //串口初始化
	ShellInit();
	BreathLed_Init();  /*呼吸灯初始化*/
	if(NandflashInit())
	{
		LOG_E("NandFlash init err\r\n");
	}
	while(1)
	{
		delay_ms(500);
	}
}


/*nandflash测试*/
void ReadNandFlashID(void)
{
	uint32_t uiID;
	NandReadID(&uiID);
	LOG_I("uiID = 0x%x\r\n",uiID);
}
RTT_INIT_EXPORT_0(ReadNandFlashID);

void EraseChip(void)
{
	uint8_t ucRet = 0;
	ucRet = NandEraseChip();
	if(ucRet == NAND_SUCCESS)
	{
		LOG_I("EraseChip succ");
	}
	else
	{
		LOG_E("EraseChip err");
	}
}
RTT_INIT_EXPORT_0(EraseChip);

void ReadFlash(int iPage, int iCol, int iSize)
{
	uint8_t ucRet = 0;
	uint8_t *pucBuff = NULL;
	pucBuff = (uint8_t *)malloc(iSize);

	ucRet = NandReadPage(iPage, iCol, pucBuff, iSize);

	if(ucRet == NAND_SUCCESS)
	{
		for (uint32_t i = 0; i < iSize; i++)
		{
			rt_kprintf("0x%x ",pucBuff[i]);
			if((i%10 == 0) && (i != 0))
			{
				rt_kprintf("\r\n");
			}
			delay_us(20);
		}
		rt_kprintf("\r\n");
		LOG_I("ReadFlash succ");
	}
	else
	{
		LOG_E("ReadFlash err");
	}
	free(pucBuff);
}
RTT_INIT_EXPORT_3(ReadFlash);

void WriteFlash(int iPage, int iCol, int iSize)
{
	uint8_t ucRet = 0;
	uint8_t *pucBuff = NULL;
	pucBuff = (uint8_t *)malloc(iSize);
	for (uint32_t i = 0; i < iSize; i++)
	{
		pucBuff[i] = i;
	}

	ucRet = NandWritePage(iPage, iCol, pucBuff, iSize);
	if(ucRet == NAND_SUCCESS)
	{
		LOG_I("WriteFlash succ");
	}
	else
	{
		LOG_E("WriteFlash err");
	}
	free(pucBuff);
}
RTT_INIT_EXPORT_3(WriteFlash);

void CopyPage(int iSrc, int iDst)
{
	uint8_t ucRet = 0;
	ucRet = NandCopyNoWrite(iSrc, iDst);
	if(ucRet == NAND_SUCCESS)
	{
		LOG_I("Copy succ");
	}
	else
	{
		LOG_E("Copy err");
	}
}
RTT_INIT_EXPORT_2(CopyPage);

void CopyWritePage(int iSrc, int iDst, int iCol)
{
	uint8_t ucRet = 0;
	uint8_t aucBuf[10] = {0xaa, 0xcd, 0x45};
	ucRet = NandCopyWriteData(iSrc, iDst, iCol, aucBuf, sizeof(aucBuf));
	if(ucRet == NAND_SUCCESS)
	{
		LOG_I("Copy write succ");
	}
	else
	{
		LOG_E("Copy write err");
	}
}
RTT_INIT_EXPORT_3(CopyWritePage);

