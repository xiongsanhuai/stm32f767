#ifndef __NANDFLASH_H
#define __NANDFLASH_H
#include <stdint.h>

#include "delay.h"

typedef struct _NandAttribute_T
{
    uint32_t uiId;                  //nandflashID
    uint32_t uiPageTotalSize;       //页总大小，包括main区和spare区
    uint32_t uiPageMainSize;        //每页main区大小
    uint32_t uiPageSpareSize;       //每页spare区大小
    uint32_t uiOneBlockPageSize;    //每个块包含的页数量
    uint32_t uiOnePlaneBlockSize;   //每个plane包含的块数量
    uint32_t uiBlockSize;     	    //总的块数量
    uint32_t uiGoodBlockSize;      	//好块数量
    uint32_t uiValidBlockSize;     	//有效块数量(供文件系统使用的好块数量)
	uint32_t uiECCData;				//硬件计算出来的ECC值
	uint32_t *puiECCHardBuff;        //ECC硬件计算值缓冲区
	uint32_t *puiECCReadBuff;       //ECC读取的值缓冲区
}NandAttribute_T,*pNandAttribute_T;

typedef volatile uint8_t NandVu8;

#define NAND_PRINTF(...)
#define NAND_LOG_D			LOG_D
#define NAND_MALLOC         malloc
#define NAND_CALLOC         calloc

//MPU相关设置
#define NAND_REGION_NUMBER      MPU_REGION_NUMBER3	    //NAND FLASH使用region0
#define NAND_ADDRESS_START      0X80000000              //NAND FLASH区的首地址
#define NAND_REGION_SIZE        MPU_REGION_SIZE_256MB   //NAND FLASH区大小

#define NAND_MAX_PAGE_SIZE			4096		//定义NAND FLASH的最大的PAGE大小（不包括SPARE区），默认4096字节
#define NAND_ECC_SECTOR_SIZE		512			//执行ECC计算的单元大小，默认512字节

#define NAND_DELAY_MS(ms)          	delay_ms(ms)
#define NAND_DELAY_US(us)        	delay_us(us)

//NAND FLASH操作相关延时函数
#define NAND_TADL_DELAY				30			//tADL等待延迟,最少70ns
#define NAND_TWHR_DELAY				25			//tWHR等待延迟,最少60ns
#define NAND_TRHW_DELAY				35			//tRHW等待延迟,最少100ns
#define NAND_TPROG_DELAY			200			//tPROG等待延迟,典型值200us,最大需要700us
#define NAND_TBERS_DELAY			4			//tBERS等待延迟,典型值3.5ms,最大需要10ms

#define NAND_ADDRESS			0X80000000	//nand flash的访问地址,接NCE3,地址为:0X8000 0000
#define NAND_CMD				1<<16		//发送命令
#define NAND_ADDR				1<<17		//发送地址

#define NAND_PAGE_MAIN_SIZE     (2048)
#define NAND_PAGE_SPARE_SIZE    (64)
#define NAND_PAGE_SIZE          (NAND_PAGE_MAIN_SIZE + NAND_PAGE_SPARE_SIZE)

#define NAND_PAGE_IN_BLOCK      (64)
#define NAND_BLOCK_IN_PLANE     (2048)
#define NAND_TOTAL_BLOCK_NB     (4096)
#define NAND_TOTAL_PAGE_NB      (0x20000)

#define NAND_ECC_SIZE           (4)
#define NAND_ECC_START          (12)
#define NAND_ID                 (0XDC909556)
//NAND FLASH命令
#define NAND_READID         	0X90    	//读ID指令
#define NAND_FEATURE			0XEF    	//设置特性指令
#define NAND_RESET          	0XFF    	//复位NAND
#define NAND_READSTA        	0X70   	 	//读状态
#define NAND_AREA_A         	0X00
#define NAND_AREA_TRUE1     	0X30
#define NAND_WRITE0        	 	0X80
#define NAND_WRITE_TURE1    	0X10
#define NAND_ERASE0        	 	0X60
#define NAND_ERASE1         	0XD0
#define NAND_MOVEDATA_CMD0  	0X00
#define NAND_MOVEDATA_CMD1  	0X35
#define NAND_MOVEDATA_CMD2  	0X85
#define NAND_MOVEDATA_CMD3  	0X10

#define NAND_SUCCESS            0x00
#define NAND_ERR                0x01
#define NAND_TIMOUT             0x02


//NAND FLASH状态
#define NSTA_READY       	   	0X40		//nand已经准备好
#define NSTA_ERROR				0X01		//nand错误
#define NSTA_TIMEOUT        	0X02		//超时
#define NSTA_ECC1BITERR       	0X03		//ECC 1bit错误
#define NSTA_ECC2BITERR       	0X04		//ECC 2bit以上错误

#define NAND_FLASH_RB           HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_6)//NAND Flash的闲/忙引脚

uint8_t NandflashInit(void);
void NAND_MPU_Config(void);

/***********nandflash操作接口*************/

pNandAttribute_T GetNandAttribute(void);

void NandAttributeInit(void);
/**
 * @brief NANDFLASH复位
 *
 * @return 0：ok err:other
 */
int NandReset(void);

/**
 * @brief 设置模式
 *
 * @param ucMode 模式
 * @return uint8_t
 */
uint8_t NandModeSet(uint8_t ucMode);

/**
 * @brief nandflash ID读取
 *
 * @param puiID 读取ID
 *
 * @return 0：ok other:err
 *
 * @note tWHR至少60ns, nandflash有五个字节，这里舍弃了最低位
 */
int NandReadID(uint32_t *puiID);

/**
 * @brief 块擦除
 *
 * @param uiBlkNum 擦除block号
 * @return 0:ok other:err
 */
uint8_t NandEraseBlock(uint32_t uiBlkNum);

/**
 * @brief 擦除全片
 *
 * @return 0:ok other:err
 */
uint8_t NandEraseChip(void);

/**
 * @brief nandflash读取页
 *
 * @param uiPage 页地址
 * @param uiColNum 该页的列地址
 * @param pBuf 读取数据存储的缓存
 * @param uiReadSize 读取数据的大小
 * @return 0：success other:err
 * @note
 * 读取页地址需要需要先发送一个command 然后发送五次地址，
 * 具体地址参看数据手册，唯一要注意一点都的是，列地址和块地址，
 * 这两个数据可以写成一个，因为1 block = 64page,页地址用了六根
 * 地址线（该nandflash地址线位*8），正好2^6 = 64，高两位用在了
 * 块地址，假如输入uiPage64，那么块地址正好为0，若输入uiPage为96，
 * 那么块地址为1，页地址为32，这是一个很巧妙的设计。
 * 页读取不允许跨页读取，但是可以读取该页的spare区，1page = 2k+64bytes(data+spare)
 * 发送了读取指令和地址后，至少需要等待50ns,然后在读取R/B（ready/busy）。
 */
uint8_t NandReadPage(uint32_t uiPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiReadSize);

/**
 * @brief 读取数据和比较值进行比较
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param ucCmpValue 比较值
 * @param uiReadSize 读取大小
 * @param puiCmpEqual 与比较值相同的个数
 * @return 0:success ohter:err
 * @note 不能跨页读取
 */
uint8_t NandReadPageCmp(uint32_t uiPage, uint32_t uiColNum, uint8_t ucCmpValue, uint32_t uiReadSize, uint32_t *puiCmpEqual);

/**
 * @brief 写入数据
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param pBuf 写入数据缓存
 * @param uiWriteSize 写入数据大小
 * @return 0:success other:err
 */
uint8_t NandWritePage(uint32_t uiPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiWriteSize);

/**
 * @brief 写入固定数据
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param uiWriteSize 写入大小
 * @param ucValue 写入数据
 * @return 0:success other:err
 */
uint8_t NandWritePageVal(uint32_t uiPage, uint32_t uiColNum, uint32_t uiWriteSize, uint8_t ucValue);

/**
 * @brief 内部数据拷贝
 *
 * @param uiSrcPage 源页地址
 * @param uiDstPage 目标地址
 * @return 0:success other:err
 */
uint8_t NandCopyNoWrite(uint32_t uiSrcPage, uint32_t uiDstPage);

/**
 * @brief 内部数据拷贝并追加数据写入
 *
 * @param uiSrcPage 源页地址
 * @param uiDstPage 目标页地址
 * @param uiColNum 追加写入数据列
 * @param pBuf 写入数据
 * @param uiWriteSize 写入数据大小
 * @return 0:success other:err
 */
uint8_t NandCopyWriteData(uint32_t uiSrcPage, uint32_t uiDstPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiWriteSize);

/**
 * @brief 读spare区数据
 *
 * @param uiPage 页地址
 * @param uiSpareColNum spare列地址
 * @param pBuf 读出数据缓存
 * @param uiReadSize 数据大小
 * @return 0:success other:err
 */
uint8_t NandReadSpare(uint32_t uiPage, uint32_t uiSpareColNum, uint8_t *pBuf, uint32_t uiReadSize);

/**
 * @brief 写入spare区数据
 *
 * @param uiPage 页地址
 * @param uiSpareColNum spare列地址
 * @param pBuf 写入数据
 * @param uiWriteSize 数据大小
 * @return 0:success other:err
 */
uint8_t NandWriteSpare(uint32_t uiPage, uint32_t uiSpareColNum, uint8_t *pBuf, uint32_t uiWriteSize);

/***********nandflash内部操作接口*************/

/**
 * @brief nandflash延时
 *
 * @param uiTim 延时时间
 *
 * @note i++至少4ns
 */
static void NandDelay(uint32_t uiTim);

/**
 * @brief 读取nandflash状态
 *
 * @return 返回状态
 *
 * @note bit7:写保护 0：protected 1:not protected
 * bit6:执行状态 0:busy 1:ready
 * bit5~1 don't care
 * bit0 操作状态 0:succ 1:err
 */

static uint8_t NandReadSta(void);
/**
 * @brief nandflash等待就绪
 *
 * @return 0：ok other:err
 */
static uint8_t NandWaitReady(void);

#endif

