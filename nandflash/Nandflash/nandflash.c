/**
 * @file nandflash.c
 * @author XSH
 * @brief
 * @version 0.1
 * @date 2023-07-09
 *
 * @copyright Copyright (c) 2023
 * @note nandflash每块（block）的第一页和第二页的spare区的第一个字节用来表示该块是否为坏块。
 *       假如第一个字节（下标为0）是0xAA表示坏块，0xFF表示好块。
 *       spare区的[1:11]用来给FTL设计使用，[12:28]用来储存Ecc。
 */

#define DBG_TAG "nandflsh"
#include "dbg.h"
#include "sys.h"
#include "nandflash.h"



NAND_HandleTypeDef NAND_Handler;    //NAND FLASH句柄
static NandAttribute_T g_stNandAttribute = {0};


uint8_t NandflashInit(void)
{
	uint8_t ucRet;
    FMC_NAND_PCC_TimingTypeDef ComSpaceTiming,AttSpaceTiming;
    NAND_MPU_Config();
    NAND_Handler.Instance=FMC_Bank3;
    NAND_Handler.Init.NandBank=FMC_NAND_BANK3;                          //NAND挂在BANK3上
    NAND_Handler.Init.Waitfeature=FMC_NAND_PCC_WAIT_FEATURE_DISABLE;    //关闭等待特性
    NAND_Handler.Init.MemoryDataWidth=FMC_NAND_PCC_MEM_BUS_WIDTH_8;     //8位数据宽度
    NAND_Handler.Init.EccComputation=FMC_NAND_ECC_DISABLE;              //禁止ECC
    NAND_Handler.Init.ECCPageSize=FMC_NAND_ECC_PAGE_SIZE_512BYTE;       //ECC页大小为512字节
    NAND_Handler.Init.TCLRSetupTime=7;                                  //设置TCLR(tCLR=CLE到RE的延时)=(TCLR+TSET+2)*THCLK,THCLK=1/180M=4.6ns
    NAND_Handler.Init.TARSetupTime=7;                                   //设置TAR(tAR=ALE到RE的延时)=(TAR+TSET+1)*THCLK,THCLK=1/180M=4.6n。

   /*command 操作习惯时序*/
    ComSpaceTiming.SetupTime=3;			//建立时间 CLE setup time tCLS = 10ns
    ComSpaceTiming.WaitSetupTime=4;		//等待时间 RE 和 WE保持低电平时间
    ComSpaceTiming.HoldSetupTime=3;		//保持时间 CLE hold time tCLH = 5ns
    ComSpaceTiming.HiZSetupTime=3;		//高阻态时间 Data Setup time = 5ns

    /*数据相关时序*/
    AttSpaceTiming.SetupTime=3;			//建立时间 ALE setup time tCLS = 10ns
    AttSpaceTiming.WaitSetupTime=4;		//等待时间 RE 和 WE保持低电平时间
    AttSpaceTiming.HoldSetupTime=3;		//保持时间 ALE hold time tCLH = 5ns
    AttSpaceTiming.HiZSetupTime=3;		//高阻态时间 Data Setup time = 5ns

    HAL_NAND_Init(&NAND_Handler,&ComSpaceTiming,&AttSpaceTiming);
	NandAttributeInit();
    ucRet = NandReset();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Nandflash reset err");
        return NAND_ERR;
    }
	NAND_DELAY_MS(100);
    ucRet = NandModeSet(4); //设置为MODE4,高速模式
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Mode set err");
        return NAND_ERR;
    }
	return NAND_SUCCESS;
}

//NAND FALSH底层驱动,引脚配置，时钟使能
//此函数会被HAL_NAND_Init()调用
void HAL_NAND_MspInit(NAND_HandleTypeDef *hnand)
{
    GPIO_InitTypeDef GPIO_Initure;

    __HAL_RCC_FMC_CLK_ENABLE();             //使能FMC时钟
    __HAL_RCC_GPIOD_CLK_ENABLE();           //使能GPIOD时钟
    __HAL_RCC_GPIOE_CLK_ENABLE();           //使能GPIOE时钟
    __HAL_RCC_GPIOG_CLK_ENABLE();           //使能GPIOG时钟

	//初始化PD6 R/B引脚
	GPIO_Initure.Pin=GPIO_PIN_6;
    GPIO_Initure.Mode=GPIO_MODE_INPUT;          //输入
    GPIO_Initure.Pull=GPIO_PULLUP;    			//上拉
    GPIO_Initure.Speed=GPIO_SPEED_HIGH;         //高速
    HAL_GPIO_Init(GPIOD,&GPIO_Initure);

	//初始化PG9 NCE3引脚
    GPIO_Initure.Pin=GPIO_PIN_9;
    GPIO_Initure.Mode=GPIO_MODE_AF_PP;          //输入
    GPIO_Initure.Pull=GPIO_NOPULL;    			//上拉
    GPIO_Initure.Speed=GPIO_SPEED_HIGH;         //高速
	GPIO_Initure.Alternate=GPIO_AF12_FMC;       //复用为FMC
    HAL_GPIO_Init(GPIOG,&GPIO_Initure);

    //初始化PD0,1,4,5,11,12,14,15
    GPIO_Initure.Pin=GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5|\
                     GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_14|GPIO_PIN_15;
    GPIO_Initure.Pull=GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD,&GPIO_Initure);

    //初始化PE7,8,9,10
    GPIO_Initure.Pin=GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
    HAL_GPIO_Init(GPIOE,&GPIO_Initure);
}

//配置MPU的region
void NAND_MPU_Config(void)
{
	MPU_Region_InitTypeDef MPU_Initure;

	HAL_MPU_Disable();							//配置MPU之前先关闭MPU,配置完成以后在使能MPU

	//配置RAM为region1，大小为256MB，此区域可读写
	MPU_Initure.Enable=MPU_REGION_ENABLE;			//使能region
	MPU_Initure.Number=NAND_REGION_NUMBER;			//设置region，NAND使用的region0
	MPU_Initure.BaseAddress=NAND_ADDRESS_START;		//region基地址
	MPU_Initure.Size=NAND_REGION_SIZE;				//region大小
	MPU_Initure.SubRegionDisable=0X00;
	MPU_Initure.TypeExtField=MPU_TEX_LEVEL0;
	MPU_Initure.AccessPermission=MPU_REGION_FULL_ACCESS;	//此region可读写
	MPU_Initure.DisableExec=MPU_INSTRUCTION_ACCESS_ENABLE;	//允许读取此区域中的指令
	MPU_Initure.IsShareable=MPU_ACCESS_NOT_SHAREABLE;
	MPU_Initure.IsCacheable=MPU_ACCESS_NOT_CACHEABLE;
	MPU_Initure.IsBufferable=MPU_ACCESS_BUFFERABLE;
	HAL_MPU_ConfigRegion(&MPU_Initure);

	HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);			//开启MPU
}

pNandAttribute_T GetNandAttribute(void)
{
    return &g_stNandAttribute;
}

void NandAttributeInit(void)
{
    g_stNandAttribute.uiId = NAND_ID;
    g_stNandAttribute.uiPageTotalSize = NAND_PAGE_SIZE;
    g_stNandAttribute.uiPageMainSize = NAND_PAGE_MAIN_SIZE;
    g_stNandAttribute.uiPageSpareSize = NAND_PAGE_SPARE_SIZE;
    g_stNandAttribute.uiOneBlockPageSize = NAND_PAGE_IN_BLOCK;
    g_stNandAttribute.uiOnePlaneBlockSize = NAND_BLOCK_IN_PLANE;
    g_stNandAttribute.uiBlockSize = NAND_TOTAL_BLOCK_NB;
    g_stNandAttribute.puiECCHardBuff = NAND_CALLOC(1, sizeof(uint32_t) * NAND_MAX_PAGE_SIZE / NAND_ECC_SECTOR_SIZE);
    g_stNandAttribute.puiECCReadBuff = NAND_CALLOC(1, sizeof(uint32_t) * NAND_MAX_PAGE_SIZE / NAND_ECC_SECTOR_SIZE);
    //TODO

}
/**
 * @brief nandflash延时
 *
 * @param uiTim 延时时间
 *
 * @note i++至少4ns
 */
static void NandDelay(uint32_t uiTim)
{
    while (uiTim > 0)
    {
        uiTim--;
    }
}

/***********nandflash操作接口*************/

/**
 * @brief 读取nandflash状态
 *
 * @return 返回状态
 *
 * @note bit7:写保护 0：protected 1:not protected
 * bit6:执行状态 0:busy 1:ready
 * bit5~1 don't care
 * bit0 操作状态 0:succ 1:err
 */
static uint8_t NandReadSta(void)
{
    uint8_t ucData = 0;
    *(uint8_t *)(NAND_ADDRESS|NAND_CMD)=NAND_READSTA;//发送读状态命令
    NandDelay(NAND_TWHR_DELAY);		//等待tWHR,再读取状态寄存器
    ucData = *(uint8_t *)NAND_ADDRESS;
    return ucData;
}

/**
 * @brief nandflash等待就绪
 *
 * @return 0：ok other:err
 */
static uint8_t NandWaitReady(void)
{
    uint8_t ucSta;
	uint32_t uiTim = 0;
    while (1)
    {
        ucSta = NandReadSta();
        if(ucSta & NSTA_READY)
        {
            break;
        }
        uiTim++;
        if(uiTim>=0X1FFFFFFF)
        {
            return NAND_TIMOUT;
        }
    }
    return NAND_SUCCESS;
}

/**
 * @brief 等待R/B信号
 *
 * @param ucSta 信号状态
 * @return 0：success other:err
 */
static int NandWaitRB(uint8_t ucSta)
{
    uint32_t uiTim = 0;
    while (uiTim < 0xFFFFFF)
    {
        uiTim++;
        if(NAND_FLASH_RB == ucSta)
        {
            return NAND_SUCCESS;
        }
    }
    return NAND_TIMOUT;
}

/**
 * @brief 获取ECC的奇数位/偶数位
 *
 * @param oe 奇偶数 0：偶数位 1：奇数位
 * @param eccval 输入的ecc值
 * @return 计算后的ecc值(最多16位)
 */
uint16_t NAND_ECC_Get_OE(uint8_t oe,uint32_t eccval)
{
	uint8_t i;
	uint16_t ecctemp=0;
	for(i=0;i<24;i++)
	{
		if((i%2)==oe)
		{
			if((eccval>>i)&0X01)ecctemp+=1<<(i>>1);
		}
	}
	return ecctemp;
}


//ECC校正函数
//eccrd:读取出来,原来保存的ECC值
//ecccl:读取数据时,硬件计算的ECC只
//返回值:0,错误已修正
//    其他,ECC错误(有大于2个bit的错误,无法恢复)
/**
 * @brief ECC校正函数
 *
 * @param data_buf 读取的数据
 * @param eccrd 读取出来,原来保存的ECC值
 * @param ecccl 读取数据时,硬件计算的ECC值
 * @return 0：错误已修正 other:ECC错误(有大于2个bit的错误,无法恢复)
 */
uint8_t NAND_ECC_Correction(uint8_t* data_buf,uint32_t eccrd,uint32_t ecccl)
{
	uint16_t eccrdo,eccrde,eccclo,ecccle;
	uint16_t eccchk=0;
	uint16_t errorpos=0;
	uint32_t bytepos=0;
	eccrdo=NAND_ECC_Get_OE(1,eccrd);	//获取eccrd的奇数位
	eccrde=NAND_ECC_Get_OE(0,eccrd);	//获取eccrd的偶数位
	eccclo=NAND_ECC_Get_OE(1,ecccl);	//获取ecccl的奇数位
	ecccle=NAND_ECC_Get_OE(0,ecccl); 	//获取ecccl的偶数位
	eccchk=eccrdo^eccrde^eccclo^ecccle;
	if(eccchk==0XFFF)	//全1,说明只有1bit ECC错误
	{
		errorpos=eccrdo^eccclo;
		NAND_LOG_D("errorpos:%d\r\n",errorpos);
		bytepos=errorpos/8;
		data_buf[bytepos]^=1<<(errorpos%8);
	}else				//不是全1,说明至少有2bit ECC错误,无法修复
	{
		NAND_LOG_D("2bit ecc error or more\r\n");
		return 1;
	}
	return 0;
}

/**
 * @brief NANDFLASH复位
 *
 * @return 0：ok err:other
 */
int NandReset(void)
{
    *(uint8_t *)(NAND_ADDRESS|NAND_CMD) = NAND_RESET;	//复位NAND
    return NandWaitReady();
}

/**
 * @brief 设置模式
 *
 * @param ucMode 模式
 * @return uint8_t
 */
uint8_t NandModeSet(uint8_t ucMode)
{
    uint8_t ucRet  = 0;
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_FEATURE;//发送设置特性命令
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=0X01;		//地址为0X01,设置mode
 	*(NandVu8*)NAND_ADDRESS=ucMode;					//P1参数,设置mode
	*(NandVu8*)NAND_ADDRESS=0;
	*(NandVu8*)NAND_ADDRESS=0;
	*(NandVu8*)NAND_ADDRESS=0;
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Mode set err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief nandflash ID读取
 *
 * @param puiID 读取ID
 *
 * @return 0：ok other:err
 *
 * @note tWHR至少60ns, nandflash有五个字节，这里舍弃了最低位
 */
int NandReadID(uint32_t *puiID)
{
    uint8_t aucDta[5] = {0};
    *(uint8_t *)(NAND_ADDRESS|NAND_CMD) = NAND_READID;	//读取ID命令
    *(uint8_t *)(NAND_ADDRESS|NAND_ADDR) = 0x00;
    NandDelay(15);
	aucDta[0] = *(NandVu8 *)(NAND_ADDRESS);
	aucDta[1] = *(NandVu8 *)(NAND_ADDRESS);
	aucDta[2] = *(NandVu8 *)(NAND_ADDRESS);
	aucDta[3] = *(NandVu8 *)(NAND_ADDRESS);
	aucDta[4] = *(NandVu8 *)(NAND_ADDRESS);
	*puiID = ((uint32_t)aucDta[1])<<24|((uint32_t)aucDta[2])<<16|((uint32_t)aucDta[3])<<8|aucDta[4];
    NAND_LOG_D("Device ID :0x%X",*puiID);
    return NAND_SUCCESS;
}

/**
 * @brief 块擦除
 *
 * @param uiBlkNum 擦除block号
 * @return 0:ok other:err
 */
uint8_t NandEraseBlock(uint32_t uiBlkNum)
{
    if(uiBlkNum > NAND_TOTAL_BLOCK_NB)
    {
        NAND_LOG_D("Erase block size err");
        return NSTA_ERROR;
    }
    //向右移6位是因为第六位是页地址，具体可查看数据手册。
    uiBlkNum <<= 6;
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_ERASE0;
    //发送块地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiBlkNum;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiBlkNum>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiBlkNum>>16);
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_ERASE1;
    NAND_DELAY_MS(4);
    if(NandWaitReady() != NAND_SUCCESS)
    {
        NAND_LOG_D("Erase block err");
        return NSTA_ERROR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 擦除全片
 *
 * @return 0:ok other:err
 */
uint8_t NandEraseChip(void)
{
    uint8_t ucRet = 0;
    for (uint32_t i = 0; i < NAND_TOTAL_BLOCK_NB; i++)
    {
        ucRet = NandEraseBlock(i);
        if(ucRet != 0)
        {
            NAND_LOG_D("Erase chip err in %d block",i);
            return NAND_ERR;
        }
    }
    return NAND_SUCCESS;
}

/**
 * @brief nandflash读取页
 *
 * @param uiPage 页地址
 * @param uiColNum 该页的列地址
 * @param pBuf 读取数据存储的缓存
 * @param uiReadSize 读取数据的大小
 * @return 0：success other:err
 * @note
 * 读取页地址需要需要先发送一个command 然后发送五次地址，
 * 具体地址参看数据手册，唯一要注意一点都的是，列地址和块地址，
 * 这两个数据可以写成一个，因为1 block = 64page,页地址用了六根
 * 地址线（该nandflash地址线位*8），正好2^6 = 64，高两位用在了
 * 块地址，假如输入uiPage64，那么块地址正好为0，若输入uiPage为96，
 * 那么块地址为1，页地址为32，这是一个很巧妙的设计。
 * 页读取不允许跨页读取，但是可以读取该页的spare区，1page = 2k+64bytes(data+spare)
 * 发送了读取指令和地址后，至少需要等待50ns,然后在读取R/B（ready/busy）。
 */
uint8_t NandReadPage(uint32_t uiPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiReadSize)
{
    uint8_t ucRet = 0;
    uint32_t uiEccNum = 0;
    uint32_t uiEccStart = 0;
    uint32_t uiEccAddr = 0;
    uint8_t *puiReadEcc = NULL;
    if(uiPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("Read page size err");
        return NAND_ERR;
    }
    if(uiColNum > NAND_PAGE_SIZE)
    {
        NAND_LOG_D("Read page col size err");
        return NAND_ERR;
    }
    if(uiColNum + uiReadSize > NAND_PAGE_SIZE) //不允许跨页
    {
        NAND_LOG_D("Read page spread pages err");
        return NAND_ERR;
    }
    *(NandVu8 *)(NAND_ADDRESS|NAND_CMD) = NAND_AREA_A;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uint8_t)(uiColNum&0xff);
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uint8_t)((uiColNum>>8)&0xff);
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uint8_t)(uiPage&0xff);
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uint8_t)((uiPage>>8)&0xff);
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uint8_t)((uiPage>>16)&0xff);
    *(NandVu8 *)(NAND_ADDRESS|NAND_CMD) = NAND_AREA_TRUE1;

    NandDelay(12); /*约为12*5 = 60ns*/
    if(NAND_SUCCESS != NandWaitRB(1))
    {
        NAND_LOG_D("Read page timeout");
        return NAND_TIMOUT;
    }
    if(uiReadSize % NAND_ECC_SECTOR_SIZE) /*不进行校验*/
    {
        /*读取NAND FLASH中的值*/
		for(uint32_t i=0; i < uiReadSize; i++)
		{
			*(NandVu8*)pBuf++ = *(NandVu8*)NAND_ADDRESS;
		}
    }
    else
    {
        uiEccNum=uiReadSize/NAND_ECC_SECTOR_SIZE;			//得到ecc计算次数
		uiEccStart=uiColNum/NAND_ECC_SECTOR_SIZE;
		for(uint32_t uiNb=0; uiNb<uiEccNum; uiNb++)
		{
			SCB_CleanInvalidateDCache();					//清除无效的D-Cache
			FMC_Bank3->PCR|=1<<6;							//使能ECC校验
			for(uint32_t i=0; i < NAND_ECC_SECTOR_SIZE; i++)				//读取NAND_ECC_SECTOR_SIZE个数据
			{
				*(NandVu8*)pBuf++ = *(NandVu8*)NAND_ADDRESS;
			}
			while(!(FMC_Bank3->SR&(1<<6)));					//等待FIFO空
			g_stNandAttribute.puiECCHardBuff[uiNb+uiEccStart]=FMC_Bank3->ECCR;//读取硬件计算后的ECC值
			FMC_Bank3->PCR&=~(1<<6);						//禁止ECC校验
		}
		uiEccAddr=NAND_PAGE_MAIN_SIZE + NAND_ECC_START + uiEccStart * NAND_ECC_SIZE;			//从spare区的0X10位置开始读取之前存储的ecc值
		NandDelay(NAND_TRHW_DELAY);//等待tRHW
		*(NandVu8*)(NAND_ADDRESS|NAND_CMD)=0X05;				//随机读指令
		//发送地址
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiEccAddr;
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiEccAddr>>8);
		*(NandVu8*)(NAND_ADDRESS|NAND_CMD)=0XE0;				//开始读数据
		NandDelay(NAND_TWHR_DELAY);//等待tWHR
		puiReadEcc = (uint8_t*)&g_stNandAttribute.puiECCReadBuff[uiEccStart];
		for(uint8_t i = 0; i < NAND_ECC_SIZE * uiEccNum; i++)								//读取保存的ECC值
		{
			*(NandVu8*)puiReadEcc= *(NandVu8*)NAND_ADDRESS;
			puiReadEcc++;
		}
		for(uint8_t i = 0;i < uiEccNum; i++)								//检验ECC
		{
			if(g_stNandAttribute.puiECCHardBuff[i + uiEccStart] != \
                g_stNandAttribute.puiECCReadBuff[i + uiEccStart])//不相等,需要校正
			{
				NAND_LOG_D("err hd,rd:0x%x,0x%x\r\n",g_stNandAttribute.puiECCHardBuff[i+uiEccStart], \
                                                    g_stNandAttribute.puiECCReadBuff[i+uiEccStart]);
                NAND_LOG_D("Read Page ecc appear err");
                NAND_LOG_D("Page:%d,colNum:%d",uiPage, uiColNum);
                NAND_LOG_D("Hard ECC:0x%x, read ECC:0x%x",g_stNandAttribute.puiECCHardBuff[i+uiEccStart], g_stNandAttribute.puiECCReadBuff[i+uiEccStart]);
				ucRet=NAND_ECC_Correction(puiReadEcc + NAND_ECC_SECTOR_SIZE * i,\
                                    g_stNandAttribute.puiECCReadBuff[i+uiEccStart], \
                                    g_stNandAttribute.puiECCHardBuff[i+uiEccStart]);//ECC校验
				if(ucRet)//2BIT及以上ECC错误
                {
                    return NSTA_ECC2BITERR;
                }
				else//1BIT ECC错误
                {
                    NAND_LOG_D("Read page have 1bit ecc");
                }
			}
		}
    }
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Read page wait ready err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 读取数据和比较值进行比较
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param ucCmpValue 比较值
 * @param uiReadSize 读取大小
 * @param puiCmpEqual 与比较值相同的个数
 * @return 0:success ohter:err
 * @note 不能跨页读取
 */
uint8_t NandReadPageCmp(uint32_t uiPage, uint32_t uiColNum, uint8_t ucCmpValue, uint32_t uiReadSize, uint32_t *puiCmpEqual)
{
    uint8_t ucRet = 0;
    uint32_t i = 0;
    if(uiPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("Read page compare size err");
        return NAND_ERR;
    }
    if(uiColNum > NAND_PAGE_SIZE)
    {
        NAND_LOG_D("Read page compare col size err");
        return NAND_ERR;
    }
    if(uiColNum + uiReadSize > NAND_PAGE_SIZE) //不允许跨页
    {
        NAND_LOG_D("Read page compare spread pages err");
        return NAND_ERR;
    }
    *(NandVu8 *)(NAND_ADDRESS|NAND_CMD) = NAND_AREA_A;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = uiColNum&0xff;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uiColNum>>8)&0xff;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = uiPage&0xff;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uiPage>>8)&0xff;
    *(NandVu8 *)(NAND_ADDRESS|NAND_ADDR) = (uiPage>>16)&0xff;
    *(NandVu8 *)(NAND_ADDRESS|NAND_CMD) = NAND_AREA_TRUE1;

    NandDelay(12); /*约为12*5 = 60ns*/
    ucRet = NandWaitRB(1);
    if(ucRet != NAND_SUCCESS)
    {
        return NAND_TIMOUT;
    }
    for (i = 0; i < uiReadSize; i++)
    {
        if(ucCmpValue != *(NandVu8*)NAND_ADDRESS)
        {
            *puiCmpEqual = i;
            NAND_LOG_D("Read data isn't equal in %d column", i);
            return NAND_ERR;
        }
    }
    *puiCmpEqual = i;
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Read page compare wait ready err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 写入数据
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param pBuf 写入数据缓存
 * @param uiWriteSize 写入数据大小
 * @return 0:success other:err
 */
uint8_t NandWritePage(uint32_t uiPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiWriteSize)
{
    uint8_t ucRet = 0;
    uint32_t uiEccNum = 0;
    uint32_t uiEccStart = 0;
    uint32_t uiEccAddr = 0;
    uint8_t *puiReadEcc = NULL;
    if(uiPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("Write page size err");
        return NAND_ERR;
    }
    if(uiColNum > NAND_PAGE_SIZE)
    {
        NAND_LOG_D("Write col size err");
        return NAND_ERR;
    }
    if(uiColNum + uiWriteSize > NAND_PAGE_SIZE) //不允许跨页
    {
        NAND_LOG_D("Write page compare spread pages err");
        return NAND_ERR;
    }

    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_WRITE0;
    //发送地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiColNum;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiColNum>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiPage>>16);
    NandDelay(NAND_TADL_DELAY);//等待tADL
    if(uiWriteSize % NAND_ECC_SECTOR_SIZE != 0) //不进行ECC
    {
        for (uint32_t i = 0; i < uiWriteSize; i++)
        {
            *(NandVu8*)NAND_ADDRESS=*((NandVu8*)pBuf);
			pBuf++;
        }
    }
    else
    {
        uiEccNum=uiWriteSize/NAND_ECC_SECTOR_SIZE;			//得到ecc计算次数
		uiEccStart=uiColNum/NAND_ECC_SECTOR_SIZE;
        for(uint32_t i = 0; i<uiEccNum; i++)
		{
			SCB_CleanInvalidateDCache();					//清除无效的D-Cache
			FMC_Bank3->PCR|=1<<6;							//使能ECC校验
			for(uint32_t j = 0;j < NAND_ECC_SECTOR_SIZE; j++)				//写入NAND_ECC_SECTOR_SIZE个数据
			{
				*(NandVu8*)NAND_ADDRESS=*((NandVu8*)pBuf);
				pBuf++;
			}
			while(!(FMC_Bank3->SR&(1<<6)));					//等待FIFO空
			g_stNandAttribute.puiECCHardBuff[i+uiEccStart]=FMC_Bank3->ECCR;	//读取硬件计算后的ECC值
  			FMC_Bank3->PCR&=~(1<<6);						//禁止ECC校验
		}
        uiEccAddr = NAND_PAGE_MAIN_SIZE + NAND_ECC_START + uiEccStart * NAND_ECC_SIZE;			//计算写入ECC的spare区地址
		NandDelay(NAND_TADL_DELAY);//等待
		*(NandVu8*)(NAND_ADDRESS|NAND_CMD)=0X85;				//随机写指令
		//发送地址
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiEccAddr;
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiEccAddr>>8);
		NandDelay(NAND_TADL_DELAY);//等待tADL
		puiReadEcc=(uint8_t *)&g_stNandAttribute.puiECCHardBuff[uiEccStart];
		for(uint8_t i = 0;i < uiEccNum; i++)					//写入ECC
		{
			for(uint8_t j = 0;j < NAND_ECC_SIZE; j++)
			{
				*(NandVu8*)NAND_ADDRESS = *(NandVu8*)puiReadEcc++;
			}
		}
	}
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_WRITE_TURE1;
	NAND_DELAY_US(NAND_TPROG_DELAY);	//等待tPROG
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Write page wait ready err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 写入固定数据
 *
 * @param uiPage 页地址
 * @param uiColNum 列地址
 * @param uiWriteSize 写入大小
 * @param ucValue 写入数据
 * @return 0:success other:err
 */
uint8_t NandWritePageVal(uint32_t uiPage, uint32_t uiColNum, uint32_t uiWriteSize, uint8_t ucValue)
{
    uint8_t ucRet = 0;
    if(uiPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("Write data size err");
        return NAND_ERR;
    }
    if(uiColNum > NAND_PAGE_SIZE)
    {
        NAND_LOG_D("Write data col size err");
        return NAND_ERR;
    }
    if(uiColNum + uiWriteSize > NAND_PAGE_SIZE) //不允许跨页
    {
        NAND_LOG_D("Write data compare spread pages err");
        return NAND_ERR;
    }

    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_WRITE0;
    //发送地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiColNum;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiColNum>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiPage>>16);
    NandDelay(NAND_TADL_DELAY);//等待tADL
    ucRet = NandWaitRB(1);
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Write data timeout");
        return NAND_TIMOUT;
    }
    if(uiWriteSize % NAND_ECC_SECTOR_SIZE != 0) //不进行ECC
    {
        for (uint32_t i = 0; i < uiWriteSize; i++)
        {
            *(NandVu8*)NAND_ADDRESS = ucValue;
        }

    }
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_WRITE_TURE1;
	NAND_DELAY_US(NAND_TPROG_DELAY);	//等待tPROG
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Write data wait ready err");
        return NAND_ERR;
    }
	return NAND_SUCCESS;
}

/**
 * @brief 内部数据拷贝
 *
 * @param uiSrcPage 源页地址
 * @param uiDstPage 目标地址
 * @return 0:success other:err
 */
uint8_t NandCopyNoWrite(uint32_t uiSrcPage, uint32_t uiDstPage)
{
    uint8_t ucRet = 0;
    if(uiSrcPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("uiSrcPage err");
        return NAND_ERR;
    }
    if(uiDstPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("uiDstPage err");
        return NAND_ERR;
    }
    if(uiSrcPage/NAND_PAGE_IN_BLOCK%2 != uiDstPage/NAND_PAGE_IN_BLOCK%2)
    {
        NAND_LOG_D("Copy page plane err");
        return NAND_ERR;
    }
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD0;	//发送命令0X00
    //发送源页地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiSrcPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiSrcPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiSrcPage>>16);
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD1;//发送命令0X35
    //发送页内列地址
	NandDelay(NAND_TADL_DELAY);//等待tADL
	ucRet = NandWaitRB(1);
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("copy data timeout");
        return NAND_TIMOUT;
    }
	if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Copy page timeout");
        return NAND_TIMOUT;
    }
    *(vu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD2;  //发送命令0X85
    //发送目的页地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiDstPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiDstPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiDstPage>>16);
	*(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD3;		//发送命令0X10
	NAND_DELAY_US(NAND_TPROG_DELAY);	//等待tPROG
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Copy page wait ready err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 内部数据拷贝并追加数据写入
 *
 * @param uiSrcPage 源页地址
 * @param uiDstPage 目标页地址
 * @param uiColNum 追加写入数据列
 * @param pBuf 写入数据
 * @param uiWriteSize 写入数据大小
 * @return 0:success other:err
 */
uint8_t NandCopyWriteData(uint32_t uiSrcPage, uint32_t uiDstPage, uint32_t uiColNum, uint8_t *pBuf, uint32_t uiWriteSize)
{
    uint8_t ucRet = 0;
    uint32_t uiEccNum = 0;
    uint32_t uiEccStart = 0;
    uint32_t uiEccAddr = 0;
    uint8_t *puiReadEcc = NULL;
    if(uiSrcPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("uiSrcPage err");
        return NAND_ERR;
    }
    if(uiDstPage > NAND_TOTAL_PAGE_NB)
    {
        NAND_LOG_D("uiDstPage err");
        return NAND_ERR;
    }
    if(uiSrcPage/NAND_PAGE_IN_BLOCK%2 != uiDstPage/NAND_PAGE_IN_BLOCK%2)
    {
        NAND_LOG_D("Copy page plane err");
        return NAND_ERR;
    }
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD0;	//发送命令0X00
    //发送源页地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)0;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiSrcPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiSrcPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiSrcPage>>16);
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD1;//发送命令0X35
    NandDelay(NAND_TADL_DELAY);
    ucRet = NandWaitRB(1);
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Copy page write timeout");
        return NAND_TIMOUT;
    }
    *(vu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD2;  //发送命令0X85
    //发送目的页地址
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiColNum;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiColNum>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiDstPage;
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiDstPage>>8);
    *(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiDstPage>>16);

    //发送页内列地址
	NandDelay(NAND_TADL_DELAY);//等待tADL
	if(uiWriteSize%NAND_ECC_SECTOR_SIZE)//不是NAND_ECC_SECTOR_SIZE的整数倍，不进行ECC校验
	{
		for(uint32_t i = 0; i < uiWriteSize; i++)		//写入数据
		{
			*(NandVu8*)NAND_ADDRESS=*(NandVu8*)pBuf++;
		}
	}else
	{
		uiEccNum = uiWriteSize/NAND_ECC_SECTOR_SIZE;			//得到ecc计算次数
		uiEccStart = uiColNum/NAND_ECC_SECTOR_SIZE;
 		for(uint32_t i = 0; i < uiEccNum; i++)
		{
			SCB_CleanInvalidateDCache();					//清除无效的D-Cache
			FMC_Bank3->PCR|=1<<6;							//使能ECC校验
			for(uint32_t j = 0;j < NAND_ECC_SECTOR_SIZE; j++)				//写入NAND_ECC_SECTOR_SIZE个数据
			{
				*(NandVu8*)NAND_ADDRESS=*(NandVu8*)pBuf++;
			}
			while(!(FMC_Bank3->SR&(1<<6)));					//等待FIFO空
			g_stNandAttribute.puiECCHardBuff[i+uiEccStart]=FMC_Bank3->ECCR;	//读取硬件计算后的ECC值
 			FMC_Bank3->PCR&=~(1<<6);						//禁止ECC校验
		}
        uiEccAddr = NAND_PAGE_MAIN_SIZE + NAND_ECC_START + uiEccStart * NAND_ECC_SIZE;			//计算写入ECC的spare区地址
		NandDelay(NAND_TADL_DELAY);//等待
		*(NandVu8*)(NAND_ADDRESS|NAND_CMD)=0X85;				//随机写指令
		//发送地址
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)uiEccAddr;
		*(NandVu8*)(NAND_ADDRESS|NAND_ADDR)=(uint8_t)(uiEccAddr>>8);
		NandDelay(NAND_TADL_DELAY);//等待tADL
		puiReadEcc=(uint8_t *)&g_stNandAttribute.puiECCHardBuff[uiEccStart];
		for(uint8_t i = 0;i < uiEccNum; i++)					//写入ECC
		{
			for(uint8_t j = 0;j < NAND_ECC_SIZE; j++)
			{
				*(NandVu8*)NAND_ADDRESS = *(NandVu8*)puiReadEcc++;
			}
		}
	}
    *(NandVu8*)(NAND_ADDRESS|NAND_CMD)=NAND_MOVEDATA_CMD3;		//发送命令0X10
 	NAND_DELAY_US(NAND_TPROG_DELAY);	//等待tPROG
    ucRet = NandWaitReady();
    if(ucRet != NAND_SUCCESS)
    {
        NAND_LOG_D("Copy page write wait ready err");
        return NAND_ERR;
    }
    return NAND_SUCCESS;
}

/**
 * @brief 读spare区数据
 *
 * @param uiPage 页地址
 * @param uiSpareColNum spare列地址
 * @param pBuf 读出数据缓存
 * @param uiReadSize 数据大小
 * @return 0:success other:err
 */
uint8_t NandReadSpare(uint32_t uiPage, uint32_t uiSpareColNum, uint8_t *pBuf, uint32_t uiReadSize)
{
    if(uiSpareColNum > NAND_PAGE_SPARE_SIZE)
    {
        NAND_LOG_D("Read spare column err");
        return NAND_ERR;
    }
    if(uiSpareColNum + uiReadSize > NAND_PAGE_SPARE_SIZE)
    {
        NAND_LOG_D("Read spare range err");
        return NAND_ERR;
    }
    return NandReadPage(uiPage, NAND_PAGE_MAIN_SIZE+uiSpareColNum, pBuf, uiReadSize);
}

/**
 * @brief 写入spare区数据
 *
 * @param uiPage 页地址
 * @param uiSpareColNum spare列地址
 * @param pBuf 写入数据
 * @param uiWriteSize 数据大小
 * @return 0:success other:err
 */
uint8_t NandWriteSpare(uint32_t uiPage, uint32_t uiSpareColNum, uint8_t *pBuf, uint32_t uiWriteSize)
{
    if(uiSpareColNum > NAND_PAGE_SPARE_SIZE)
    {
        NAND_LOG_D("Write spare column err");
        return NAND_ERR;
    }
    if(uiSpareColNum + uiWriteSize > NAND_PAGE_SPARE_SIZE)
    {
        NAND_LOG_D("Write spare range err");
        return NAND_ERR;
    }
    return NandWritePage(uiPage, NAND_PAGE_MAIN_SIZE+uiSpareColNum, pBuf, uiWriteSize);
}
